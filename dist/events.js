(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["Events"] = factory();
	else
		root["Events"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "dist";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	var Inverse = __webpack_require__(1);
	var observable = function observable(el) {
	
	    el = el || {};
	
	    /**
	     * Используется в zManagerOfEvents для перехвата всех событий
	     */
	    el.onAll = undefined;
	
	    var callbacks = {},
	        _id = 0;
	
	    el.on = function (events, fn) {
	        if (typeof fn == 'function') {
	            fn._id = typeof fn._id == 'undefined' ? _id++ : fn._id;
	
	            events.replace(/\S+/g, function (name, pos) {
	                (callbacks[name] = callbacks[name] || []).push(fn);
	                fn.typed = pos > 0;
	            });
	        }
	        return el;
	    };
	
	    el.off = function (events, fn) {
	        if (events == '*') callbacks = {};else {
	            events.replace(/\S+/g, function (name) {
	                if (fn) {
	                    var arr = callbacks[name];
	                    for (var i = 0, cb; cb = arr && arr[i]; ++i) {
	                        if (cb._id == fn._id) {
	                            arr.splice(i, 1);i--;
	                        }
	                    }
	                } else {
	                    callbacks[name] = [];
	                }
	            });
	        }
	        return el;
	    };
	
	    // only single event supported
	    el.one = function (name, fn) {
	        function on() {
	            el.off(name, on);
	            fn.apply(el, arguments);
	        }
	        return el.on(name, on);
	    };
	
	    //todo spec
	    //при подписке на данное событие, все остальные подписчики удаляются.
	    el.single = function (events, fn) {
	        el.off(events);
	        return el.on(events, fn);
	    };
	
	    el.trigger = function (name) {
	        var args = [].slice.call(arguments, 1),
	            fns = callbacks[name] || [];
	
	        if (el.onAll !== undefined) {
	            el.onAll(name, args);
	        }
	
	        for (var i = 0, fn; fn = fns[i]; ++i) {
	            if (!fn.busy) {
	                fn.busy = 1;
	                fn.apply(el, fn.typed ? [name].concat(args) : args);
	                if (fns[i] !== fn) {
	                    i--;
	                }
	                fn.busy = 0;
	            }
	        }
	
	        if (callbacks.all && name != 'all') {
	            el.trigger.apply(el, ['all', name].concat(args));
	        }
	
	        return el;
	    };
	
	    return el;
	};
	
	var Events = {};
	
	/*
	 * Теперь Events имеет следуюие события:
	 *  - on
	 *  - off
	 *  - one
	 *  - single
	 *  - trigger
	 */
	observable(Events);
	
	/**
	 * observable  позволит предоставить интерфейс подписок для других объектов.
	 * @type {observable}
	 */
	Events.observable = observable;
	
	/**
	 * Inverse  позволит создавать контейнеры.
	 * @type {Inverse}
	 */
	Events.Inverse = Inverse;
	
	/**
	 * Глобальный контейнер
	 */
	Events.container = new Inverse();
	
	if (typeof window !== 'undefined' && window.Events === undefined) {
	    //Используется для тестирования
	    window.Events = Events;
	}
	
	module.exports = Events;

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;(function (root, factory) {
	    if (true) !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	    else if (typeof exports === 'object') module.exports = factory();
	    else root.Inverse = factory();
	})(this, function () {
	    'use strict';
	    
	    var Inverse = function() {
	        this._boundCallbacks = {};
	        this._singletonCallbacks = {};
	        this._instantiatedSingletons = {};
	        this._registeredObjects = {};
	    };
	    
	    Inverse.prototype.make = function(name) {
	        if (this._registeredObjects.hasOwnProperty(name)) {
	            return this._registeredObjects[name];
	        }
	        
	        var args = Array.prototype.slice.call(arguments, 1);
	        
	        if (this._singletonCallbacks.hasOwnProperty(name)) {
	            if (!this._instantiatedSingletons.hasOwnProperty(name)) {
	                this._instantiatedSingletons[name] = this._singletonCallbacks[name].apply(this, args);
	            }
	            
	            return this._instantiatedSingletons[name];
	        }
	        
	        if (this._boundCallbacks.hasOwnProperty(name)) {
	            return this._boundCallbacks[name].apply(this, args);
	        }
	        
	        return null;
	    };
	    
	    Inverse.prototype.bind = function(name, callback) {
	        this._boundCallbacks[name] = callback;
	    };
	    
	    Inverse.prototype.singleton = function(name, callback) {
	        this._singletonCallbacks[name] = callback;
	    };
	    
	    Inverse.prototype.register = function(name, object) {
	        this._registeredObjects[name] = object;
	    };
	    
	    return Inverse;
	});


/***/ }
/******/ ])
});
;
//# sourceMappingURL=events.js.map